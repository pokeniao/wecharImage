import os


# 破壳鸟
# 校验图片类型
def image_type_check(buf):
    types = {
        'jpg': (0xff, 0xd8),
        'png': (0x89, 0x50),
        'gif': (0x47, 0x49)
    }
    for imageType in types:
        OneByte, TwoByte = types[imageType]
        key = OneByte ^ buf[0]
        if key ^ buf[1] == TwoByte:
            return key, imageType


# 图片输出
def out_image(key, file, path, buf):
    # 创建目录
    save_root = os.getcwd() + "/转换图片/" + path
    if not os.path.exists(save_root):
        os.makedirs(save_root)

    save_path = os.path.join(save_root, file)

    new_buf = bytearray()
    for b in buf:
        new_buf.append(b ^ key)
    with open(save_path, 'wb') as wb:
        wb.write(new_buf)


# 遍历目录方法
def find_dat_files(directory):
    print("执行中")
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".dat") and "Image" in root:
                # 创建二进制输入输出流
                with open(os.path.join(root, file), 'rb') as rb:
                    buf = bytearray(rb.read())
                key, image_type = image_type_check(buf)
                file_name, file_extension = os.path.splitext(file)
                file_name += "." + image_type
                save_root = root.replace(directory, "")
                out_image(key, file_name, save_root, buf)
                print(f"转换文件{os.path.join(root, file)}完成")


if __name__ == '__main__':
    import sys

    if len(sys.argv) == 2:
        directory = sys.argv[1]
        find_dat_files(directory)
    else:
        print("请输入路径地址")
        print("路径地址如:python wxImage.py \"C:/Users/admin/Documents/WeChat "
              "Files/wxid_7z9auxu675rv12/FileStorage/MsgAttach\"")
        print("路径打开微信设置中缓存位置找到。注意:路径用引号包裹")